# Change log

## 0.3.0

### New features

* fuzzy: add `fuzzy` option for fuzzier matching logic (1448d434dcf1810a437865a3c32191efea5a1a5a)

## 0.2.5

### Other changes

* ci: update test node versions (08525e2e814f0ea59d73df24214b4c373d6f7f08)
* deps: upgrade deps (37ff6aacf1d3d5fe36ddfc523ce4b153dc97eaa1)
* ci: don't test in node 4 (c616002fad4bdd0e0c9038c3239a3a8db764b6fb)
* project: remove travis config (a8d000a746fbff0eefc246419dbbafcb2e853d0d)

## 0.2.4

### Other changes

* project: migrate to gitlab (548a05b)

## 0.2.3

* fix: update the readme (55fc4cd)
* fix: update the example code (cfeb182)
* refactor: rename loop index variable (a7b44a1)
* fix: ensure final-token mismatches fail the query (905cb55)

## 0.2.2

* fix: use cloned subquery array in recursive calls to filter

## 0.2.1

* fix: ensure correct documentId is propagated for fuzzy matches

## 0.2.0

* feat: implement `coerceId` option

## 0.1.1

* fix: sanely handle unicode documents and queries

## 0.1.0

* feat: implement `index.clear`

## 0.0.1

* feat: add readme
* fix: repair broken fuzzy-matching
* refactor: convert N_GRAMS to a Map instance
* refactor: eliminate explicit loop continue

## 0.0.0

* feat: implement `surch.create`
* feat: implement `index.add`
* feat: implement `index.delete`
* feat: implement `index.update`
* feat: implement `index.search`

